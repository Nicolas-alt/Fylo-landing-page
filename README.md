# Frontend Mentor - Fylo landing page with dark theme and features grid


## Welcome! 👋

Thanks for checking out this front-end coding challenge.

[Frontend Mentor](https://www.frontendmentor.io) challenges allow you to improve your skills in a real-life workflow.


<img src="./assets/images/desktop-design.jpg"   alt="Result image" />

**To do this challenge you need a basic understanding of HTML and CSS.**

## [Final result here!](https://nicolas-alt.vercel.app)
